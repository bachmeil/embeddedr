// Allocate a matrix from D, manipulate it from D and R, and pass it between D and R
// It's all efficient because the only thing passed between the languages is a pointer
import embeddedR.r;

void main() {
	auto m = RMatrix(2,2);
	
	// R does not fill a matrix by default, so these elements are garbage
	m.print("Matrix allocated by R, not filled by default, so it contains garbage");
	
	// There are obviously better ways to do this, but librd only
	// provides a barebones library for communicating with R.
	// You use it to get the matrix data into D, not to work with the matrix.
	m[0,0] = 1.5;
	m[0,1] = 2.5;
	m[1,0] = 3.5;
	m[1,1] = 4.5;
	m.print("Matrix allocated by R, but filled in D");
	
	// Pass the pointer to the R object directly to R using .robj
	m.robj.toR("rm");
	
	// More conveniently, use this form
	m.toR("rm");
	
	// Print the matrix in R
	// First calls the R API function for printing an R object
	// Second prints the object after it's been passed to R
	printR(m.robj);
	evalRQ(`print(rm)`);
	
	// For efficiency, we're passing pointers between R and D
	// That means changes to the matrix in R also affect the RMatrix in D
	m[0,0] = 5.5;
	m.print("Matrix after it has been changed in D");
	evalRQ(`print(rm)`);
	
	// Changes in R generally do not affect m, however, because R may create a copy when changing
	evalRQ(`rm[1,1] <- 7.5`);
	evalRQ(`print(rm)`);
	m.print("Now rm refers to something different in R, because a copy was made, so it has a different pointer");
}
