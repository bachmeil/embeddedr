import embedr.r;
import std.stdio;

void main() {
	evalRQ(`a <- 4L`);
	evalRQ(`b <- 4.5`);
	evalRQ(`d <- "hello world"`);
	
	// Have to explicitly declare scalars, because R doesn't
	// have a scalar type, just vectors with one element.
	auto a = evalR("a").scalar!int;
	auto a2 = evalR("a").scalar!long;
	double b = evalR("b").scalar;
	auto d = evalR("d").scalar!string;
	
	writeln(a);
	writeln(a2);
	writeln(b);
	writeln(d);
	
	// Less verbose version where the evalR part is done for you
	writeln(scalar!int("a"));
	writeln(scalar!long("a"));
	writeln(scalar("b"));
	writeln(scalar!string("d"));
	
	["foo", "bar", "baz"].toR("dstring");
	printR("dstring");
	
	evalRQ(`rstring <- c("under", "the", "bridge")`);
	writeln(stringArray("rstring"));
}
