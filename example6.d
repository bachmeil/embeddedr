import embeddedR.r;
import std.stdio;

void main() {
	evalRQ(`rl <- list(a=rnorm(15), b=matrix(rnorm(100), ncol=4))`);
	auto dl = NamedList("rl");
	dl.print;
	auto dlm = RMatrix(dl["b"]);
	dlm.print("This is the matrix from that list");
	auto dlv = RVector(dl["a"]);
	dlv.print("This is the vector from that list");
	NamedList nl;
	nl["a"] = dlm.data;
	nl["b"] = dlv.data;
	nl.toR("rl2");
	
	// Now see that this list has the elements reversed
	printR("rl2");
}
