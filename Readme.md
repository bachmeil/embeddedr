# Overview

embeddedR is a library that allows you to embed an R interpreter inside a D program. Data is passed between the two languages using Dirk Eddelbuettel's RInside package. Data is passed efficiently, because the only thing that is passed between the languages is a pointer to an object allocated in R.

embeddedR also provides wrappers around R API functions that allow you to work with, and allocate, R objects. Structs are provided to make it more convenient to work with matrix, vector, list, and (what is on the D side) scalar R objects. These structs are reference counted to deal with the R garbage collector. The user of this library should never have to deal with, or even know anything about, protecting/unprotecting objects from the R garbage collector.

# Tutorial

There is a tutorial available in the file calling-r-from-d.md.

# Usage

Embedding an R interpreter in your program is simple. Put the file r.d in your project directory, add `import embeddedR.r;` to your source file, and you are done. There are seven example files demonstrating the usage of the embeddedR library.

# Compilation

Compilation is not quite as easy because you have to link to three C libraries. On Linux, these are

- libR.so
- libRInside.so
- librinsided.so

Compilation should be similar on other OSes, but I don't have access to anything other than Linux, so I can't be of any more help than that.

libR.so is installed when you install R. libRInside.so is installed when you install the RInside package in R. librinsided.so is a C interface to libRInside.so, which is a C++ library.

On my Ubuntu system, compilation of example1.R looks like this when librinsided.so is in the current working directory (note that the .so files have to be placed in this order):

```
dmd example1.d r.d -L/usr/lib/libR.so -Llibrinsided.so -L/usr/local/lib/R/site-library/RInside/lib/libRInside.so
```

Because librinsided.so is located in the current working directory, I run the example with

```
LD_LIBRARY_PATH=. ./example1
```

You may want to create a script, use Dub, create a Makefile, etc. in order to automate this process. I have created an example script called ex.d. To compile and run example5.d, I run `rdmd ex.d 5`. If you use that file, you will have to set the two variables at the top.

# Installation On Linux

1. Install R.
2. Install the RInside package in R: `install.packages("RInside")`.
3. Compile librinsided.cpp. The easy way to do this is to run cpplib.d: `rdmd cpplib.d`. That should locate the required headers and libraries and create librinsided.so. If for some reason that fails (you can ignore messages like "/bin/sh: 2: -Wl,--export-dynamic: not found"), you can set the variables as described in the file.
    
    Alternatively, you can do everything manually. You have to link to libRInside.so and libR.so, and include RInside.h, Rcpp.h, and R.h. On my Ubuntu system, that is done with:
    
    ```
    g++ -c -fPIC librinsided.cpp -L/usr/lib64/libRInside.so -I/usr/lib64/R/library/RInside/include/ -I/usr/lib64/R/library/Rcpp/include/ -I/usr/lib64/R/include/ -L/usr/lib64/R/lib/libR.so
    ```
    
    ```
    gcc -shared -Wl,-soname,librinsided.so -o librinsided.so librinsided.o
    ```

4. Copy librinsided.so into your project directory or into a directory like /usr/lib.

# License

My code is released under your choice of the GPLv2, GPLv3, or the Boost License 1.0. Note that this only applies to the code I have written. R is itself released under the GPL, and any programs using this library link to R, so you will have to work through the relevant licensing issues if you want to distribute in binary form a program that uses embeddedR.

# Will You Provide A Dub Package?

No. But remember that this is free software, so you have the freedom make a Dub package and share it with your friends if that's what floats your boat.

# Bugs/Suggestions/Feedback/Questions

Please open an issue. The more issues, the better. My dream is to create an open source project that reaches 1000 issues.

# Contributions

Contributions are always welcome. The current version of the library is feature complete, and I currently have no plans to release a new version, so a contribution would take the form of documentation or examples.

# Stability

I've used much of this library extensively in my research over the last three years. It is possible that there are bugs in some of the lesser-used parts. However, I have tested everything, so there shouldn't be too many bugs (yes, I know how silly that sounds).
