import embeddedR.r;
import std.stdio;

void main() {
	auto v = RVector([1.1, 2.2, 3.3, 4.4, 5.5]);
	v.toR("rv");
	printR("rv");
	
	// Advantage of a vector over a matrix with one column is
	// that you can use foreach to iterate over the elements
	foreach(val; v) {
		writeln(val);
	}
}
