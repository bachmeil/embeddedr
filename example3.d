import embeddedR.r;

void main() {
	evalRQ(`m <- matrix(rnorm(100), ncol=5)`);
	
	// This form of the constructor pulls m from R into D and creates and RMatrix
	auto dm = RMatrix("m");
	dm.print("This is a matrix that was created in R");
	
	// Change one element
	dm[0,0] = dm[0,0]*4.5;
	
	// If you send a string as an argument to printR, it prints the given variable
	printR("m");
}
