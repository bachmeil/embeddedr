import std.conv, std.exception, std.process, std.stdio, std.string;

// Path to librinsided.so
// It's wherever you put it
// Default is the current directory
string librinsided = "/home/office/R/x86_64-pc-linux-gnu-library/3.2/embedr/libs";

void main(string[] args) {
  enforce(args.length > 1, "You have to tell me which example to run");
  
  // Installation directory for RInside
  // Can set rinsidedir manually if you want, but shouldn't need to
  // Can be found by running `find.package("RInside")` in R
  auto w = executeShell(`Rscript -e 'cat(find.package("RInside"))'`);
  string rinsidedir = w.output.to!string;
  writeln("Found RInside package in ", rinsidedir);

  // Path to libR.so
  // Can be found with `locate libR.so`
  // Note that it is common to have multiple libR.so installed
  // If you set this manually, you need only the path to libR.so
  w = executeShell(`locate -b '\libR.so' -l 1`);
  string libr = strip(w.output.to!string);
  writeln("Found libR.so in ", libr);

  string cmd = "dmd example" ~ args[1] ~ ".d r.d -L" ~ libr ~ " -L" ~ librinsided ~ "/librinsided.so -L" ~ rinsidedir ~ "/lib/libRInside.so ";
  writeln(cmd);
  auto v = executeShell(cmd);
  writeln(v.output);
  
  cmd = "LD_LIBRARY_PATH=" ~ librinsided ~ " ./example" ~ args[1];
  writeln(cmd);
  v = executeShell(cmd);
  writeln(v.output);
}
